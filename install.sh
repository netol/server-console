#!/bin/bash
  ## Script for installing visible light Xorg environment on phisical DISPLAY=:0 attached to the server, where some informations will be displayed, eg. IP addresses, network and usage monitoring,  In this case, THE SERVER is the PROXMOX 8 distro, whit VM's inside,, accessible via private encrypted VPN (VNC)

echo "⚙️\ install/update, wait..." | tr '\n' ' '
_DATE_STAMP="$(date +%Y-%m-%d--%H-%M-%S)"
_PROJECT="server-console"
_USER="vi"
_PASSWORD="nopassword" # means, there is no actually password for the user
DISPLAY=":0"
_RESOLUTION="1280x1024"
_WALLPAPER="minecraft-server"

--install () {
  install -m 777 -d /opt/$_PROJECT/images
  install -m 666 -C -D ./images/* /opt/$_PROJECT/images
  install -m 777 -d /opt/$_PROJECT/bin
  install -m 777 -C -D ./bin/* /opt/$_PROJECT/bin
}

--grub () {
  ## modify grub and add splash image
  _GRUB="/etc/default/grub"
  cp $_GRUB $_GRUB-$_DATE_STAMP.bak && sed -i '/#/d' $_GRUB && sed -i '/^[[:space:]]*$/d' $_GRUB ## backup grub config & delete all comments
  _SPLASH="/opt/$_PROJECT/images/$_WALLPAPER.tga"
  declare -a LINES_TO_DELETE=(
    "GRUB_TIMEOUT="
    "GRUB_BACKGROUND="
    "GRUB_GFXMODE="
  )
  for _LINE in ${LINES_TO_DELETE[@]}; do
    sed -i "/"$_LINE"/d" $_GRUB
  done
  echo "GRUB_BACKGROUND=\"$_SPLASH\"" >> $_GRUB
  echo "GRUB_TIMEOUT=3" >> $_GRUB
  echo "GRUB_GFXMODE=\"$_RESOLUTION\"" >> $_GRUB
  echo "GRUB_COLOR_NORMAL=\"black/black\"" >> $_GRUB
  echo "GRUB_COLOR_HIGHLIGHT=\"black/black\"" >> $_GRUB
  echo "COLOR_NORMAL=\"black/black\"" >> $_GRUB
  echo "COLOR_HIGHLIGHT=\"black/black\"" >> $_GRUB
  update-grub 2>/dev/null && echo -e "🟢${FUNCNAME[0]} function done"
}

--apt-install () {
  apt-get install -y -qq --no-install-recommends gnupg
  _GGL_REPO="deb http://dl.google.com/linux/chrome/deb/ stable main"
  _SRC_FILE="/etc/apt/sources.list"
  wget --no-check-certificate -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/google-chrome.gpg
  grep -x -q "$_GGL_REPO" $_SRC_FILE || echo $_GGL_REPO >> $_SRC_FILE
  apt update
  declare -a STUFF=(
  "sudo"
  "xorg"
  "openbox"
  "xdotool"
  "x11vnc"
  "wmctrl"
  "xcompmgr"
  "feh"
  "bc"
  ## "ntp" coflict witch chrony
  "xclip"
  "ntpdate"
  "scrot"
  "tint2"
  "htop"
  "tmux"
  "google-chrome-stable"
  )
  for _PACKAGE in ${STUFF[@]}; do
    echo "♻ ️\ ... install package: $_PACKAGE ............................. wait ..." | tr '\n' ' '
    apt-get install -y -qq --no-install-recommends $_PACKAGE && echo -e "🟢  $_PACKAGE done"
  done
  echo -e "🟢${FUNCNAME[0]} function done"
  systemctl set-default multi-user.target
  systemctl daemon-reload
}

--useradd () { #help: add user
  /usr/sbin/groupadd $_USER 2>/dev/null
  /usr/sbin/useradd -r -s /bin/bash -m -g $_USER $_USER 2>/dev/null
  /usr/sbin/usermod -a -G "cdrom,floppy,dip,plugdev,audio,video,sudo" $_USER
  _SUDO_LINE="$_USER ALL=(ALL:ALL) NOPASSWD:ALL"
  grep -x -q "$_SUDO_LINE" /etc/sudoers || echo $_SUDO_LINE >> /etc/sudoers
  _CAF="/etc/cron.allow" # allow user to use crontab
  if ! grep -q $_USER $_CAF; then
    echo "$_USER" >> $_CAF
    echo "$_USER added to $_CAF"
  else
    echo "$_USER already exists in $_CAF"
  fi
}

--autologin () { #help: set autologin, who will be automaticaly logged in
rm -R /etc/systemd/system/getty@tty1.service.d 2>/dev/null
mkdir -pv /etc/systemd/system/getty@tty1.service.d 2>/dev/null
cat > /etc/systemd/system/getty@tty1.service.d/autologin.conf << EOF
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin $_USER --noclear %I $TERM
EOF
systemctl set-default multi-user.target
systemctl daemon-reload
}

--autostart () {
  cat > /home/${_USER}/.profile << EOF
[[ \$(tty) = /dev/tty1 ]] && setterm -blank 0 -powersave off && startx
EOF
  chown $_USER:$_USER /home/$_USER/.profile
}

--configure () {
  mkdir -pv /home/${_USER}/.config/openbox/
  cp -R /etc/xdg/openbox /home/${_USER}/.config
  chown -R ${_USER}:${_USER} /home/${_USER}/.config
  rm -R /home/${_USER}/.config/openbox/autostart
  cat > /home/${_USER}/.config/openbox/autostart << EOF
# Programs that will run after Openbox windows manager has started
xrandr --output default --mode $RESOLUTION &
xrandr --output Virtual1 --mode $RESOLUTION &
feh --bg-scale /opt/${_PROJECT}/images/${_WALLPAPER}.png &
xset -dpms &
xset s off &
xcompmgr &
/opt/${_PROJECT}/bin/xterm.sh &
x11vnc -display :0 --forever &
transset -a 0.8
(sleep 2s && tint2) &
EOF
  chown ${_USER}:${_USER} /home/${_USER}/.config/openbox/*
  chmod +x /home/${_USER}/.config/openbox/autostart
  cat > /usr/share/xsessions/${_USER}.desktop << EOF
[Desktop Entry]
Version=1.0
Name=${_USER}
Exec=default
Icon=
Type=Application
EOF
  echo -e "🟢${FUNCNAME[0]} function done"
}

--help () {
  echo -e "🔮 help"; exit 0
}

case $1 in
  "$1")
    if [ -z "${1+x}" ]; then
      echo -e "🔵 [ START ] install all stuff"
      --install
      --grub
      --apt-install
      --useradd
      --autologin
      --autostart
      --configure
    fi
  $1
    if [ ! $? == "0" ]; then
      echo -e "🔴 [ERROR] $1 - function not found, try $0 --help"
    else
      echo -e "🟢 [ OK ] `pwd`$0 done"
    fi
  ;;
  "?"|"-?")
    --help
  ;;
esac


  ##
  ## Source: https://gitlab.com/netol/server-console
  ## TODO: install console without X
