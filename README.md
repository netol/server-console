Linux script for installing visible light Xorg environment on phisical DISPLAY=:0 attached to the server, where some informations will be displayed,  
eg. IP addresses, network and usage monitoring, etc.  
In this case, THE SERVER is the PROXMOX 8 distro, whith VM's inside,, the console is accessible via private encrypted VPN (VNC)

### 🟢 Install (update) from Gitlab:
```
wget https://gitlab.com/netol/server-console/-/archive/main/server-console-main.tar.gz -O - | tar -xzf -
sh server-console-main/install.sh
```

Example:  
![minecraft-server-console](./images/minecraft-server-console-5fps.gif "minecraft-server-console")
