#!/bin/bash
START_SITE="https://duckduckgo.com/?t=h_&q=minecraft&iax=images&ia=images"

run-google-chrome () {
  google-chrome --window-size="1100,1000" \
  --window-position="0,0" \
  --incognito \
  --disable-notifications \
  --mute-audio \
  --disable-device-discovery-notifications \
  --no-first-run \
  --no-default-browser-check \
  --disable-translate \
  --process-per-site \
  --disable-sync-preferences \
  --disable-gpu \
  --disable-plugins \
  --disable-plugins-discovery \
  --disable-preconnect \
  --dns-prefetch-disable \
  --no-experiments \
  --no-pings \
  --no-referrers \
  --disable-infobars \
  --disable-session-crashed-bubble \
  $START_SITE & 2>/dev/null
}

run-google-chrome
